import './App.css';
import CardList from "./components/CardList/CardList";

function App() {
  return (
    <div className="App">
        <h1>Ты сегодня покормил кота?</h1>
        <CardList></CardList>
    </div>
  );
}
export default App;
