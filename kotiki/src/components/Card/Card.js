import React from 'react';
import './Card.css';
import cat from './IMG/cat.png'

export default class Card extends React.Component {
    static defaultProps = {
        id: 0,
        composition: '',
        amount: '',
        mouse: '',
        weight: '',
        checked: false,
        avail: ''
    }

    select = (id) => {
        if (this.props.avail === 'stock'){
        let cla = id.toString();
        let idArr = ['bord', 'cycle'];
        let check = this.checked || this.props.checked;
            if (document.getElementById(cla + idArr[0])) {
                for (let i = 0; i < 2; i++) {
                    let color = document.getElementById(cla + idArr[i]);
                    switch (i) {
                        case 0:
                            if (check === false) {
                                color.style.background = 'linear-gradient(135deg, transparent 30px,  #D91667 0)';
                            } else {
                                color.style.background = 'linear-gradient(135deg, transparent 30px,  #1698D9 0)';
                            }
                            break;
                        case 1:
                            if (check === false) {
                                color.style.background = '#D91667';
                                this.checked = true;
                            } else {
                                color.style.background = '#1698D9';
                                this.checked = false;
                            }
                            break;
                        default: {
                        }
                    }
                }
            }
        }
    };
    render() {
        const { composition, amount, mouse, weight, id, avail} = this.props;
        return (
            <div className={'card'}>
                    <div className={'bord-' + avail} id={id + 'bord'} onClick={() => this.select(id)} >
                        <div className={'card-info ' + avail}>
                            <div className={'card__information'}>
                                <span>Сказочное заморское яство</span>
                                <h1>Нямушка</h1>
                                <h2>{composition}</h2>
                                <p>
                                    {amount}<br/>
                                    {mouse}
                                </p>
                            </div>
                            <div className={'card__wrapper img'}>
                                <img src={cat} alt={'cat'}/>
                            </div>
                            <div className={'cycle-' + avail} id={id + 'cycle'}>
                                <span className={'cycle__weight'}>{weight}</span>
                                <br/>
                                <span className={'cycle__kg'}>КГ</span>
                            </div>
                        </div>
                </div>
                <div className={'sub'}>
                    <p>Чего сидишь? Порадуй котэ, <span>купи</span>.</p>
                </div>
            </div>
        );
    }
}


