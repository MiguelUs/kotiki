import React from 'react';
import Card from "../Card/Card";
import './CardList.css';

export default class CardList extends React.Component {
    render() {
        const shopList = [
            {
                "id": 1,
                "composition": "с фуа-гра",
                "amount": "10 порций",
                "mouse": "мышь в подарок",
                "weight": "0,5",
                "avail": "none"
            },
            {
                "id": 2,
                "composition": "с рыбой",
                "amount": "40 порций",
                "mouse": "2 мыши в подарок",
                "weight": "2",
                "avail": "stock"
            },
            {
                "id": 3,
                "composition": "с курой",
                "amount": "100 порций",
                "mouse": "5 мышей в подарок",
                "weight": "5",
                "avail": "stock"
            }
        ]
        return (
            <div className={'card-list'}>
                {
                    shopList.map((shop) => {
                        return (
                            <div className={'card-list__item'} key={shop.id}>
                                    <Card
                                        id={shop.id}
                                        composition={shop['composition']}
                                        amount={shop['amount']}
                                        mouse={shop['mouse']}
                                        weight={shop['weight']}
                                        avail={shop['avail']}/>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}
